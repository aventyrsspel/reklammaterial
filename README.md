![logo](https://user-images.githubusercontent.com/680264/65699162-f8ba3f00-e07d-11e9-93e9-e26fd71d0a76.png)

# Äventyrsspel v1.7
File dump of now defunct [Äventyrsspel v1.7 scan repository](http://petterkatt.no-ip.org/%c3%84ventyrsspel_17/).

O===[====================-

## Reklammaterial
This repo contains all the "reklammaterial" content from the above site.

Please see the [Äventyrsspel group](https://gitlab.com/aventyrsspel) for more.
